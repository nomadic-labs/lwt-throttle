type 'a t

val make : delay:Ptime.Span.t -> action:(unit -> 'a) -> 'a t

val use : 'a t -> unit

val wait : 'a t -> 'a Lwt.t

val cancel : 'a t -> unit
