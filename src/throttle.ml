type 'a t = {
  action: unit -> 'a;
  delay: Ptime.Span.t;
  mutable throttle: 'a Lwt.t;
}

let make ~delay ~action = {action; delay; throttle = Lwt.fail Lwt.Canceled}

let use t =
  match Lwt.state t.throttle with
  | Lwt.Fail _
  | Lwt.Return _ ->
      let p =
        let open Lwt.Infix in
        Lwt_unix.sleep (Ptime.Span.to_float_s t.delay) >>= fun () ->
        Lwt.return (t.action ())
      in
      t.throttle <- p
  | Lwt.Sleep -> ()

let wait t = Lwt.protected t.throttle

let cancel t = Lwt.cancel t.throttle
